import { NextFunction, Request, Response } from 'express';
import { CustomRequest } from '../utils/custom-request';
import { RecipeService } from './recipe.service';
import * as httpStatus from 'http-status';
import * as mongoose from 'mongoose';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import { UserService } from '../user/user.service';
import { IRecipe, RecipeDocument } from './recipe.document';
import { AuctionatorService } from '../auctionator/auctionator.service';
import { AuctionatorDocument } from '../auctionator/auctionator.document';
import ObjectId = mongoose.Types.ObjectId;

export class RecipeController {
  getAll(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    RecipeService.get()
      .getAll(req.query, customReq.context.skip, customReq.context.limit)
      .then(recipes => {
        res.status(httpStatus.OK).send(recipes);
      })
      .catch(next);
  }

  create(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    let createdRecipe: IRecipe;
    RecipeService.get()
      .create({ ...req.body, createdBy: customReq.context.user._id })
      .then(recipe => {
        createdRecipe = recipe;
        (customReq.context.user.savedRecipes || (customReq.context.user.savedRecipes = [])).push(recipe._id);
        return UserService.get().update(customReq.context.user._id, {
          savedRecipes: customReq.context.user.savedRecipes
        });
      })
      .then(() => {
        res.status(httpStatus.CREATED).send(createdRecipe);
      })
      .catch(next);
  }

  search(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    RecipeService.get()
      .getAll(
        { 'item.name': new RegExp('.*' + req.query.name + '.*', 'gmi') },
        customReq.context.skip,
        customReq.context.limit
      )
      .then(recipes => {
        res.status(httpStatus.OK).send(recipes);
      })
      .catch(next);
  }

  auctionator(req: Request, res: Response, next: NextFunction): void {
    Promise.all([
      AuctionatorService.get()
        .model()
        .findById(new ObjectId(req.params.aid)),
      RecipeService.get().get(new ObjectId(req.params.id))
    ]).then(result => {
      const auctionator: AuctionatorDocument = result[0];
      const recipe: RecipeDocument = result[1];
      const tmpRecipe: any = recipe.toJSON();
      const mainItem = auctionator.data.find(i => i.itemId.toHexString() === recipe.item._id.toString());
      tmpRecipe.item.priceData = mainItem?.price;
      const resultItem = auctionator.data.find(i => i.itemId.toHexString() === recipe.itemResult._id.toString());
      tmpRecipe.itemResult.priceData = resultItem?.price;
      for (const item of tmpRecipe.components) {
        const tmpItem = auctionator.data.find(i => i.itemId.toHexString() === item._id.toString());
        item.priceData = tmpItem?.price;
      }

      res.status(httpStatus.OK).send(tmpRecipe);
    });
  }

  get(req: Request, res: Response, next: NextFunction): void {
    RecipeService.get()
      .get(new ObjectId(req.params.id), req.query)
      .then(recipe => {
        if (!recipe) {
          throw new CustomError(CustomErrorCode.ERRNOTFOUND, ' Recipe not found');
        }
        res.status(httpStatus.OK).send(recipe);
      })
      .catch(next);
  }

  update(req: Request, res: Response, next: NextFunction): void {
    RecipeService.get()
      .update(new ObjectId(req.params.id), req.body)
      .then(recipe => {
        res.status(httpStatus.OK).send(recipe);
      })
      .catch(next);
  }
}
