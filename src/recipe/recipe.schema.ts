export const RecipeCreateSchema = {
  type: 'object',
  required: ['item', 'components', 'profession'],
  additionalProperties: false,
  properties: {
    components: {
      type: 'array',
      items: {
        type: 'object',
        required: ['name', 'quantity'],
        properties: {
          quantity: {
            type: 'number'
          },
          _id: {
            type: 'string',
            pattern: '^[0-9a-fA-F]{24}$'
          },
          name: {
            type: 'string'
          },
          fixedPrice: {
            type: 'number'
          }
        }
      }
    },
    profession: {
      type: 'string'
    },
    public: {
      type: 'boolean'
    },
    item: {
      type: 'object',
      required: ['name'],
      properties: {
        _id: {
          type: 'string',
          pattern: '^[0-9a-fA-F]{24}$'
        },
        name: {
          type: 'string'
        },
        fixedPrice: {
          type: 'number'
        }
      }
    },
    itemResult: {
      type: 'object',
      required: ['name'],
      properties: {
        _id: {
          type: 'string',
          pattern: '^[0-9a-fA-F]{24}$'
        },
        name: {
          type: 'string'
        },
        fixedPrice: {
          type: 'number'
        },
        quantity: {
          type: 'number'
        }
      }
    }
  }
};

export const RecipeQuerySchema = {
  type: 'object',
  required: ['id'],
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      pattern: '^[0-9a-fA-F]{24}$'
    }
  }
};

export const RecipeUpdateSchema = {
  type: 'object',
  required: ['fixedPrice'],
  additionalProperties: false,
  properties: {
    fixedPrice: {
      type: 'number'
    }
  }
};
