import { Schema, Types } from 'mongoose';
import { ItemSchema } from '../item/item.model';
import ObjectId = Types.ObjectId;

export const RecipeItemSchema = new Schema({
  _id: {
    type: String
  },
  name: {
    type: String,
    required: true
  },
  fixedPrice: {
    type: Number
  },
  quantity: {
    type: Number,
    required: true
  }
});

export const RecipeSchema = new Schema({
  components: {
    type: [RecipeItemSchema],
    required: true,
    default: []
  },
  createdAt: { type: Date, default: Date.now, required: true },
  public: {
    type: Boolean,
    default: true
  },
  createdBy: {
    type: ObjectId,
    required: true
  },
  item: {
    type: ItemSchema,
    required: true
  },
  itemResult: {
    type: ItemSchema,
    required: true
  }
});
