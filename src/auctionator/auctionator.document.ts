import { Document, Types } from 'mongoose';
import ObjectId = Types.ObjectId;

export interface IAuctionator {
  realm: string;
  faction: string;
  data: ItemImport[];
  userId: ObjectId;
  createdAt: Date;
}

export interface AuctionatorDocument extends IAuctionator, Document {}

export interface ItemImport {
  name: string;
  itemId?: ObjectId;
  price?: number;
  idImport?: string;
}
