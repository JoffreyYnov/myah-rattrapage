export class SubscribeRequest {
  topic: string;
  userId: string;
}
