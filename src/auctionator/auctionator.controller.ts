import { AuctionatorService } from './auctionator.service';
import { NextFunction, Request, Response } from 'express';
import * as httpStatus from 'http-status';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import { CustomRequest } from '../utils/custom-request';
import * as mongoose from 'mongoose';
import * as fs from 'fs';
import { configureLogger, defaultWinstonLoggerOptions, getLogger } from '../utils/logger';
import AMQP_MANAGER, { CustomQueue } from '../amqp/amqp.manager';
import { getConfiguration } from '../helpers/configuration.helper';
import { getPackageName } from '../helpers/package.helper';
import { ConsumeMessage } from 'amqplib';
import { AuctionatorDocument, IAuctionator } from './auctionator.document';
import { WebSocketEvent } from '../web-socket/web-socket-event';
import { Alert, AlertHelper, AlertType } from '../helpers/alert.helper';
import { UserService } from '../user/user.service';
import { RecipeService } from '../recipe/recipe.service';
import { RecipeDocument } from '../recipe/recipe.document';
import ObjectId = mongoose.Types.ObjectId;

const loggerName = 'AuctionatorController';
configureLogger(loggerName, defaultWinstonLoggerOptions);
const logger = getLogger(loggerName);

const config = getConfiguration()[getPackageName()];

export interface ParseResult {
  success: boolean;
  userId: string;
  auctionator?: IAuctionator;
  err: any;
}

export class AuctionatorController {
  queueResult: CustomQueue;

  getAll(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    AuctionatorService.get()
      .getAll(req.query, customReq.context.skip, customReq.context.limit)
      .then(auctionators => {
        res.status(httpStatus.OK).send(auctionators);
      })
      .catch(next);
  }

  get(req: Request, res: Response, next: NextFunction): void {
    AuctionatorService.get()
      .get(new ObjectId(req.params.id), req.query)
      .then(auctionator => {
        if (!auctionator) {
          throw new CustomError(CustomErrorCode.ERRNOTFOUND, 'Auctionator not found');
        }
        res.status(httpStatus.OK).send(auctionator);
      })
      .catch(next);
  }

  create(req: Request, res: Response, next: NextFunction): void {
    try {
      if (!AMQP_MANAGER.queues.get(config.queueResult)) {
        logger.log('info', 'Init of auctionator controller, start listening parse output');
        AMQP_MANAGER.createQueue(config.queueResult)
          .then(customQueue => {
            this.queueResult = customQueue;
            return customQueue.channel.consume(config.queueResult, msg => this.onParseResult(msg));
          })
          .catch(err => logger.log('error', err));
      }

      const customReq = (req as any) as CustomRequest;
      const file = (req.files as any).data[0];
      const luaFile = fs.readFileSync(file.path);
      AMQP_MANAGER.createQueue(config.queueParse).then((customQueue: CustomQueue) => {
        logger.log('info', 'A message has been through the queue ' + config.queueParse);
        customQueue.sendMessage(
          Buffer.from(JSON.stringify({ userId: customReq.context.user._id.toString(), data: luaFile }), 'utf8')
        );
      });
      res.status(httpStatus.OK).send({ message: 'Import is in progress' });
    } catch (err) {
      next(err);
    }
  }

  analyse(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    const items: any[] = [];
    UserService.get()
      .get(customReq.context.user._id)
      .then(user => {
        items.push(...user.favoritesItems);
        return AuctionatorService.get().get(new ObjectId(req.params.id));
      })
      .then(auctionator => {
        const response: any = {};
        for (const item of auctionator.data) {
          const favItem = items.find(i => {
            return i.itemId.toHexString() === item.itemId.toHexString() && i.priceLimit;
          });
          if (favItem && item.price <= favItem.priceLimit) {
            (favItem as any).underPrice = true;
          }
        }

        response.auctionator = auctionator;
        response.favoritesItems = items;
        res.status(httpStatus.OK).send(response);
      })
      .catch(next);
  }

  realms(req: Request, res: Response, next: NextFunction): void {
    AuctionatorService.get()
      .getRealms()
      .then(realms => {
        res.status(httpStatus.OK).send(realms);
      })
      .catch(next);
  }

  factions(req: Request, res: Response, next: NextFunction): void {
    AuctionatorService.get()
      .getFactions()
      .then(factions => {
        res.status(httpStatus.OK).send(factions);
      })
      .catch(next);
  }

  recipeCheck(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    Promise.all([
      RecipeService.get().getAll({
        _id: customReq.context.user.savedRecipes
      }),
      AuctionatorService.get().get(new ObjectId(req.params.id))
    ])
      .then(result => {
        const recipes: RecipeDocument[] = result[0];
        const auctionator: AuctionatorDocument = result[1];
        const response: any = {
          auctionatorId: auctionator._id,
          faction: auctionator.faction,
          realm: auctionator.realm,
          recipes: []
        };
        for (const recipe of recipes) {
          const tmpRecipe: any = recipe.toJSON();
          const mainItem = auctionator.data.find(i => i.itemId.toHexString() === recipe.item._id.toString());
          tmpRecipe.item.priceData = mainItem?.price;
          const resultItem = auctionator.data.find(i => i.itemId.toHexString() === recipe.itemResult._id.toString());
          tmpRecipe.itemResult.priceData = resultItem?.price;
          for (const item of tmpRecipe.components) {
            const tmpItem = auctionator.data.find(i => i.itemId.toHexString() === item._id.toString());
            item.priceData = tmpItem?.price;
          }
          response.recipes.push(tmpRecipe);
        }

        res.send(response);
      })
      .catch(next);
  }

  onParseResult(msg: ConsumeMessage) {
    const result: ParseResult = JSON.parse(msg.content.toString());
    if (result.success) {
      WebSocketEvent.get().sendMessage(
        'notifications',
        {
          message: 'Parse succeed !',
          type: AlertType.Success,
          data: {
            component: 'dashboard-import'
          }
        },
        result.userId
      );
    } else {
      logger.log('error', JSON.stringify(result.err));
      let errFormated: Alert;
      if (result.err.name === CustomError.name) {
        errFormated = AlertHelper.fromCustomError(result.err);
        errFormated.data.component = 'dashboard-import';
      } else {
        errFormated = {
          message: result.err.message,
          title: 'Error',
          type: AlertType.Error,
          data: {
            component: 'dashboard-import',
            cause: result.err
          }
        };
      }
      WebSocketEvent.get().sendMessage('notifications', errFormated, result.userId);
    }
    this.queueResult.channel.ack(msg);
  }
}
