import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import { AuctionatorDocument, IAuctionator } from './auctionator.document';
import { Service } from '../utils/service.abstract';
import { AuctionatorSchema } from './auctionator.model';
import { getConfiguration } from '../helpers/configuration.helper';
import { configureLogger, defaultWinstonLoggerOptions, getLogger } from '../utils/logger';
import ObjectId = mongoose.Types.ObjectId;

const config: any = getConfiguration().user;
const loggerName = 'AuctionatorService';
configureLogger(loggerName, defaultWinstonLoggerOptions);
const logger = getLogger(loggerName);

export class AuctionatorService extends Service<AuctionatorDocument> {
  constructor(model: Model<AuctionatorDocument>) {
    super(model, 'auctionator');
  }

  public static get(): AuctionatorService {
    return super.getService(AuctionatorSchema, 'auctionator', 'auctionators', AuctionatorService);
  }

  async getAll(criteria: any = {}, skip = 0, limit = config.paging.defaultValue) {
    return this._model
      .find(criteria, { data: false })
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(limit);
  }

  async get(id?: ObjectId, criteria: any = {}) {
    if (id) {
      criteria._id = id;
    }
    return this._model.findOne(criteria);
  }

  async create(auctionatorData: IAuctionator) {
    logger.log('warn', 'Trying to create auctionator by user service');
    throw new CustomError(CustomErrorCode.ERRFORBIDDEN, 'Have to use the parser way');
  }

  async getRealms() {
    const realms = await this.model().aggregate([
      {
        $group: {
          _id: '$realm'
        }
      }
    ]);
    return realms.map(r => r._id);
  }

  async getFactions() {
    const factions = await this.model().aggregate([
      {
        $group: {
          _id: '$faction'
        }
      }
    ]);
    return factions.map(f => f._id);
  }
}
