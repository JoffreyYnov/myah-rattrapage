import { Schema, Types } from 'mongoose';
import Mixed = Schema.Types.Mixed;
import ObjectId = Types.ObjectId;

export const AuctionatorSchema = new Schema({
  realm: {
    type: String,
    required: true
  },
  faction: {
    type: String,
    required: true
  },
  data: {
    type: Mixed,
    required: true
  },
  userId: {
    type: ObjectId,
    required: true
  },
  createdAt: { type: Date, default: Date.now, required: true }
});
