import { Document } from 'mongoose';

export interface IItem {
  name: string;
  fixedPrice?: number;
  createdAt: Date;
  imports?: any;
}

export interface ItemDocument extends IItem, Document {}
