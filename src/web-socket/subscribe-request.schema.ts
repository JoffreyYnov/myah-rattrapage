export const SubscribeRequestSchema = {
  type: 'object',
  required: ['topic', 'userId'],
  properties: {
    topic: {
      type: 'string'
    },
    userId: {
      type: 'string',
      pattern: '^[0-9a-fA-F]{24}$'
    }
  }
};
