import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import ObjectId = mongoose.Types.ObjectId;

export interface IUser {
  _id: any;
  username: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
  createdAt: Date;
  updatedAt: Date;
  enabled: boolean;
  emailing: boolean;
  loginEnabled: boolean;
  roles: ObjectId[];
  lastLogIn: Date;
  favoritesItems: EmbeddedFavoriteItem[];
  savedRecipes: ObjectId[];
}

export interface UserDocument extends IUser, Document {}

export interface EmbeddedFavoriteItem {
  itemId: ObjectId;
  priceLimit: number;
}
